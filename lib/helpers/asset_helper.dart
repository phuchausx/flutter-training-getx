class AssetHelper {
  static const String cardImage = 'assets/images/card.png';
  static const String ex1Image = 'assets/images/ex1.png';
  static const String ex2Image = 'assets/images/ex2.png';
  static const String ex3Image = 'assets/images/ex3.png';
  static const String ex4Image = 'assets/images/ex4.png';

  static const String figureImage = 'assets/images/figure.png';
  static const String figure1Image = 'assets/images/figure1.png';
  static const String squat1Image = 'assets/images/squat1.jpg';
  static const String squat2Image = 'assets/images/squat2.jpg';
  static const String squat3Image = 'assets/images/squat3.jpg';
  static const String squat4Image = 'assets/images/squat4.jpg';
  static const String fitnessBanner =
      'assets/images/fitness-vertical-banner.jpg';
}
