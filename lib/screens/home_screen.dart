import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_training_getx/widgets/widgets.dart';
import 'package:flutter_training_getx/helpers/colors.dart' as colors;
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List info = [];
  _initData() async {
    await DefaultAssetBundle.of(context)
        .loadString("json/info.json")
        .then((value) {
      setState(() {
        info = json.decode(value);
      });
    });
  }

  @override
  void initState() {
    _initData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const TitleHomeCard(title: 'Training'),
            const SizedBox(height: 25),
            Row(
              children: [
                Text(
                  'Your program',
                  style: TextStyle(
                    color: colors.AppColor.homePageTitle,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Expanded(child: Container()),
                GestureDetector(
                  onTap: () {
                    Get.toNamed('/detail_screen');
                  },
                  child: Row(
                    children: [
                      Text(
                        'Details',
                        style: TextStyle(
                          color: colors.AppColor.homePageDetail,
                          fontSize: 20,
                        ),
                      ),
                      const SizedBox(width: 6),
                      const Icon(Icons.arrow_forward)
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20),
            const WorkoutWithVideo(
              title: 'Next workout',
              description: 'Legs Toning \nand Glutes Workout',
              minutes: 60,
            ),
            const SizedBox(height: 20),
            const StatusWorkoutCard(
              statusTitle: 'You are doing great',
              description: 'keep it up \nstick to your plan',
            ),
            const SizedBox(height: 15),
            Text(
              'Area of focus',
              style: TextStyle(
                color: colors.AppColor.homePageTitle,
                fontSize: 22,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 10),
            Expanded(
              child: GridView.builder(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 55),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 30,
                  crossAxisSpacing: 30,
                ),
                itemCount: info.length,
                itemBuilder: (BuildContext context, int index) {
                  return AreaOfFocusCard(
                    image: info[index]['img'],
                    name: info[index]['title'],
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
