import 'package:flutter/material.dart';
import 'package:flutter_training_getx/screens/detail_screen.dart';
import 'package:flutter_training_getx/screens/screens.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      getPages: [
        GetPage(name: '/', page: () => const SplashScreen()),
        GetPage(name: '/home_screen', page: () => const HomeScreen()),
        GetPage(name: '/detail_screen', page: () => const DetailScreen())
      ],
    );
  }
}
