import 'package:flutter/material.dart';
import 'package:flutter_training_getx/helpers/colors.dart' as colors;

class TitleHomeCard extends StatelessWidget {
  const TitleHomeCard({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
              color: colors.AppColor.homePageTitle,
              fontSize: 24,
              fontWeight: FontWeight.w700,
            ),
          ),
          Expanded(child: Container()),
          IconButton(
            icon: const Icon(
              Icons.calendar_month_rounded,
              color: Colors.black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
