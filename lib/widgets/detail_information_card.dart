import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_training_getx/helpers/colors.dart' as colors;

class DetailInformationCard extends StatelessWidget {
  const DetailInformationCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(
                Icons.arrow_back_ios_new,
                color: Colors.white.withOpacity(0.5),
                size: 30,
              ),
            ),
            Expanded(child: Container()),
            GestureDetector(
              onTap: () {},
              child: Icon(
                Icons.info_outline_rounded,
                color: Colors.white.withOpacity(0.5),
                size: 20,
              ),
            ),
          ],
        ),
        const SizedBox(height: 30),
        Text(
          'Legs Toning \nand Glutes Workout',
          style: TextStyle(
            color: colors.AppColor.homePageContainerTextSmall,
            fontSize: 23,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 25),
        Row(
          children: [
            Container(
              decoration: BoxDecoration(
                color: colors.AppColor.secondPageTitleColor.withOpacity(0.3),
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(6),
              child: Row(
                children: const [
                  Icon(
                    Icons.timer_sharp,
                    color: Colors.white,
                    size: 18,
                  ),
                  SizedBox(width: 5),
                  Text(
                    '68 min',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(width: 20),
            Container(
              decoration: BoxDecoration(
                color: colors.AppColor.secondPageTitleColor.withOpacity(0.3),
                borderRadius: BorderRadius.circular(6),
              ),
              padding: const EdgeInsets.all(6),
              child: Row(
                children: const [
                  Icon(
                    Icons.home_repair_service_rounded,
                    color: Colors.white,
                    size: 18,
                  ),
                  SizedBox(width: 5),
                  Text(
                    'Resistent band, Kettlebell',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
