import 'package:flutter/material.dart';
import 'package:flutter_training_getx/helpers/colors.dart' as colors;

class WorkoutWithVideo extends StatelessWidget {
  const WorkoutWithVideo({
    super.key,
    required this.title,
    required this.description,
    required this.minutes,
  });

  final String title;
  final String description;
  final num minutes;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 220,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            colors.AppColor.gradientFirst.withOpacity(0.8),
            colors.AppColor.gradientSecond.withOpacity(0.9),
          ],
          begin: Alignment.bottomLeft,
          end: Alignment.centerRight,
        ),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10),
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
          topRight: Radius.circular(80),
        ),
        boxShadow: [
          BoxShadow(
            offset: const Offset(5, 10),
            blurRadius: 20,
            color: colors.AppColor.gradientSecond.withOpacity(0.2),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: colors.AppColor.homePageContainerTextSmall,
            ),
          ),
          const SizedBox(height: 8),
          Text(
            description,
            style: TextStyle(
              color: colors.AppColor.homePageContainerTextSmall,
              fontSize: 24,
              fontWeight: FontWeight.w500,
            ),
          ),
          Expanded(child: Container()),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              const Icon(
                Icons.timer_outlined,
                color: Colors.white,
              ),
              const SizedBox(width: 10),
              Text(
                '$minutes min',
                style: TextStyle(
                    color: colors.AppColor.homePageContainerTextSmall),
              ),
              Expanded(child: Container()),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(60),
                  boxShadow: [
                    BoxShadow(
                      color: colors.AppColor.gradientFirst,
                      blurRadius: 10,
                      offset: const Offset(4, 8),
                    )
                  ],
                ),
                child: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.play_circle_fill,
                      color: Colors.white,
                      size: 42,
                    )),
              )
            ],
          )
        ],
      ),
    );
  }
}
