import 'package:flutter/material.dart';
import 'package:flutter_training_getx/helpers/asset_helper.dart';
import 'package:flutter_training_getx/helpers/colors.dart' as colors;

class StatusWorkoutCard extends StatelessWidget {
  const StatusWorkoutCard(
      {super.key, required this.statusTitle, required this.description});

  final String statusTitle;
  final String description;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 160,
      child: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 120,
            margin: const EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: const DecorationImage(
                  image: AssetImage(AssetHelper.cardImage),
                  fit: BoxFit.fill,
                ),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 40,
                    offset: const Offset(8, 10),
                    color: colors.AppColor.gradientSecond.withOpacity(0.3),
                  ),
                  BoxShadow(
                    blurRadius: 10,
                    offset: const Offset(-1, -5),
                    color: colors.AppColor.gradientSecond.withOpacity(0.3),
                  )
                ]),
          ),
          Positioned(
            top: 0,
            left: 30,
            child: Container(
              width: 100,
              height: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: const DecorationImage(
                  image: AssetImage(AssetHelper.figureImage),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Positioned(
            top: 34,
            left: 150,
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 150 - 50,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    statusTitle,
                    style: TextStyle(
                      color:
                          colors.AppColor.secondPageContainerGradient1stColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 14),
                  Text(
                    description,
                    style: TextStyle(
                      color: colors.AppColor.secondPageTopIconColor,
                      fontSize: 16,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
