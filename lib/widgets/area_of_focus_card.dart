import 'package:flutter/material.dart';
import 'package:flutter_training_getx/helpers/asset_helper.dart';
import 'package:flutter_training_getx/helpers/image_helper.dart';
import 'package:flutter_training_getx/helpers/colors.dart' as colors;

class AreaOfFocusCard extends StatelessWidget {
  const AreaOfFocusCard({
    super.key,
    required this.image,
    this.widthFactor = 0.5,
    required this.name,
  });

  final String image;
  final double widthFactor;
  final String name;

  @override
  Widget build(BuildContext context) {
    final double widthValue = MediaQuery.of(context).size.width / widthFactor;
    return Container(
      width: widthValue,
      // height: widthValue,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius: 3,
                offset: const Offset(5, 5),
                color: colors.AppColor.gradientSecond.withOpacity(0.1)),
            BoxShadow(
                blurRadius: 3,
                offset: const Offset(-5, -5),
                color: colors.AppColor.gradientSecond.withOpacity(0.1))
          ]),
      padding: const EdgeInsets.all(10),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: ImageHelper.loadFromAsset(AssetHelper.ex1Image),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              name,
              style: TextStyle(
                color: colors.AppColor.secondPageContainerGradient2ndColor,
                fontSize: 20,
              ),
            ),
          )
        ],
      ),
    );
  }
}
