import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_training_getx/helpers/colors.dart' as colors;

class TopicCard extends StatelessWidget {
  const TopicCard({
    super.key,
    required this.thumbnailVideo,
    required this.title,
    required this.executionTime,
  });

  final String thumbnailVideo;
  final String title;
  final num executionTime;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Container(
              width: 80,
              height: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  image: AssetImage(
                    thumbnailVideo,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(width: 12),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    color: colors.AppColor.secondPageContainerGradient1stColor,
                    fontSize: 17,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(height: 14),
                Text(
                  '$executionTime seconds',
                  style: TextStyle(
                    color: colors.AppColor.secondPageContainerGradient1stColor
                        .withOpacity(0.5),
                  ),
                )
              ],
            )
          ],
        ),
        const SizedBox(height: 10),
        Row(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 13,
                vertical: 5,
              ),
              decoration: BoxDecoration(
                color: colors.AppColor.loopColor.withOpacity(0.3),
                borderRadius: BorderRadius.circular(7),
              ),
              child: Text(
                '15s rest',
                style:
                    TextStyle(color: colors.AppColor.loopColor, fontSize: 12),
              ),
            ),
            const Expanded(child: DottedLine()),
          ],
        )
      ],
    );
  }
}
